Please see the [wiki](http://ufoai.sf.net) for more information on how to compile the game and the gamedata

Quickstart:

```./configure```
```make``` yes, no multiprocessing here or errors will occur.
```make lang```
```make -j$(nproc) maps```

(optional) ```make 0maps.pk3``` + ```make clean-maps``` saves 2GB.

Run: ```./ufo```.
